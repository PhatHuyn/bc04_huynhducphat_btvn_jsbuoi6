function TaoTheDivB4(){
    console.log("Bài 4 - Tạo thẻ Div");
    
    document.getElementById("content1").style.display = 'block';
    document.getElementById("content2").style.display = 'block';
    document.getElementById("content3").style.display = 'block';
    document.getElementById("content4").style.display = 'block';
    document.getElementById("content5").style.display = 'block';
    document.getElementById("content6").style.display = 'block';
    document.getElementById("content7").style.display = 'block';
    document.getElementById("content8").style.display = 'block';
    document.getElementById("content9").style.display = 'block';
    document.getElementById("content10").style.display = 'block';

    var divs = document.getElementsByClassName("bt");
    for(var i = 0; i<divs.length;i++){
        if((i+1) % 2 == 0 ){
            divs[i].style.background = 'red';
        }else{
            divs[i].style.background = 'blue';
        }
    }

    console.log("Đã tạo xong");
    console.log("---------------------------------------");
}